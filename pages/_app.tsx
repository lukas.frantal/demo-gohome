import type { FC } from 'react'
import type { AppProps } from 'next/app'
import { Styler } from '../styles'

// STYLES FROM ICOMOON
import '../components/common/Icon/style.css'

const _app: FC<AppProps> = ({ Component, pageProps }) => (
	<Styler>
		<Component {...pageProps} />
	</Styler>
)

// or
// https://callstack.com/blog/sweet-render-hijacking-with-react/
// import App from 'next/app'

// class _app extends App {
// 	render() {
// 		return <Styler>{super.render()}</Styler>
// 	}
// }

export default _app
