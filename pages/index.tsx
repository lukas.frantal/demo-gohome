import type { FC } from 'react'
import styled from 'styled-components'
import { Header, Body, Footer, Container, ContainerEnumDirection } from '../components'

const Wrapper = styled(Container)`
	height: 100%;
	background-image: ${({ theme }) =>
		`radial-gradient(${theme.colors.black} 1px, transparent 1px), radial-gradient(${theme.colors.black} 1px, transparent 1px)`};
	background-position: 0 0, 25px 25px;
	background-size: 50px 50px;
`

const IndexPage: FC = () => (
	<Wrapper direction={ContainerEnumDirection.COL}>
		<Header />
		<Body />
		<Footer />
	</Wrapper>
)

export default IndexPage
