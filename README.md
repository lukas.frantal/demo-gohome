# [Demo gohome](https://gohome.lukasfrantal.com/)

In the project I used:

- [https://vercel.com/docs](https://vercel.com/docs)
- [www.npmjs.com/package/gohome](https://www.npmjs.com/package/gohome)
- [gitlab.com/lukas.frantal/gohome](https://gitlab.com/lukas.frantal/gohome)
- [nextjs.org](https://nextjs.org)
- [reactjs.org](https://reactjs.org/)
- [www.typescriptlang.org/index.html](https://www.typescriptlang.org/index.html)
- [react-hook-form.com](https://react-hook-form.com)
- [styled-components.com](https://styled-components.com)
- [docker.com](https://docker.com)
- [eslint.org](https://eslint.org)
- [babeljs.io](https://babeljs.io)
- [prettier.io](https://prettier.io)
- [husky](https://typicode.github.io/husky/#/)
- [commitlint](https://commitlint.js.org/#/)
- [css reset](https://www.npmjs.com/package/reset-css)

## Docker commands:

### Change Docker Settings:

Command docker build

```
docker build . -t frantallukas10/demo-gohome
```

Command docker push

```
docker push frantallukas10/demo-gohome
```
