const prepareLastText = (type: string): string => {
	switch (true) {
		case type === 'startTime':
			return 'start working time! Example: 7:00'
		case type === 'maxTime':
			return 'max working time! Example: 8'
		case type === 'minTime':
			return 'min working time! Example: 6'
		case type === 'lunchTime':
			return 'lunch time! Example: 30'
		default:
			return ''
	}
}

export const createHint = (value: string, type: string): string =>
	`This value: "${value}" is wrong \n please define correct your ${prepareLastText(type)}`

export const isValidLocalStorageData = (data: string): boolean =>
	Boolean(
		// eslint-disable-next-line max-len
		/^{"startTime":"(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]","maxTime":"(0[0-9]|1[0-9]|2[0-3]|[0-9])","minTime":"(0[0-9]|1[0-9]|2[0-3]|[0-9])","lunchTime":"([0-9][0-9]|[0-9][0-9][0-9])"}$/g.exec(
			data,
		),
	)
