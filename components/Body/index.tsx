import { FC, useState, useEffect } from 'react'
import styled from 'styled-components'
import { useForm } from 'react-hook-form'
import { gohome, isValidInput } from 'gohome'
import { Results } from '../Results'
import { Links } from '../Links'
import { Container, ContainerEnumDirection, ContainerEnumPosition, Button, ButtonEnumType, Input, InputEnumType } from '../common'
import { createHint, isValidLocalStorageData } from './helper'

const Wrapper = styled(Container)`
	flex: 1;
	overflow-y: auto;
`

const WrapperInput = styled.div`
	padding-top: 5px;
	background-color: ${({ theme }) => theme.colors.white};

	${({ theme }) => theme.breakpoints.desktop} {
		padding-top: 20px;
	}
`

const Form = styled.form`
	text-align: center;
	flex: 1;
	max-width: 600px;
	width: 100%;
	padding: 10px;

	${WrapperInput} {
		&:first-child {
			padding-top: 0;
		}
	}
`

const ButtonSubmit = styled(Button)`
	max-width: 150px;
`

const ButtonReset = styled(ButtonSubmit)`
	max-width: 150px;
	border: 2px solid ${({ theme }) => theme.colors.black};
	background-color: ${({ theme }) => theme.colors.black};
	color: ${({ theme }) => theme.colors.white};

	${({ theme }) => theme.breakpoints.desktop} {
		&:hover {
			background-color: ${({ theme }) => theme.colors.white};
			color: ${({ theme }) => theme.colors.black};
		}
	}
`

const WrapperButton = styled.div`
	display: flex;
	margin: 5px 0;
	justify-content: center;
	gap: 20px;

	${({ theme }) => theme.breakpoints.tablet} {
		margin: 20px 0;
	}
`

type TimeValuesProps = {
	startTime: string
	maxTime: string
	minTime: string
	lunchTime: string
}

const initialResultsValues = {
	actual: {
		hours: '',
		minutes: '',
	},
	start: {
		hours: '',
		minutes: '',
	},
	min: {
		done: false,
		hours: '',
		minutes: '',
		saldo: {
			hours: '',
			minutes: '',
		},
	},
	max: {
		done: false,
		hours: '',
		minutes: '',
		saldo: {
			hours: '',
			minutes: '',
		},
	},
}

type GohomI = typeof initialResultsValues

export const Body: FC = () => {
	const [results, setResults] = useState<GohomI | false>(initialResultsValues)
	const [data, setData] = useState<TimeValuesProps | string | null>(null)
	const [calcInterval, setCalcInterval] = useState(false)
	const [showResults, setShowResults] = useState(false)
	const {
		register,
		handleSubmit,
		reset,
		watch,
		setValue,
		formState: { errors },
	} = useForm<TimeValuesProps>({
		defaultValues: {
			startTime: '',
			maxTime: '',
			minTime: '',
			lunchTime: '',
		},
	})

	const handleReset = (): void => {
		setResults(initialResultsValues)
		setCalcInterval(false)
		setShowResults(false)
		localStorage.clear()
		// reset value for react hoock form
		reset()
	}

	const calcGohome = ({ startTime, maxTime, minTime, lunchTime }: TimeValuesProps): void => {
		const calcResults: GohomI | false = gohome(startTime, Number(maxTime), Number(minTime), Number(lunchTime))
		setResults(calcResults)
		setShowResults(true)
	}

	const onSubmit = (values: TimeValuesProps): void => {
		calcGohome(values)
		setCalcInterval(true)
		const valuesAsString = JSON.stringify(values)
		setData(valuesAsString)
		localStorage.setItem('gohomeTime', valuesAsString)
	}

	// for load data from localStorage
	useEffect(() => {
		const dataLocalStorage: string = localStorage.getItem('gohomeTime') || ''

		// clear localStorage if data are not valid
		if (!isValidLocalStorageData(dataLocalStorage)) {
			return localStorage.clear()
		}

		if (Boolean(dataLocalStorage) && JSON.stringify(data) === JSON.stringify(dataLocalStorage)) {
			return
		}

		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const valuesParsed: TimeValuesProps = JSON.parse(dataLocalStorage)
		setValue('startTime', valuesParsed.startTime)
		setValue('maxTime', valuesParsed.maxTime)
		setValue('minTime', valuesParsed.minTime)
		setValue('lunchTime', valuesParsed.lunchTime)
		calcGohome(valuesParsed)
		setCalcInterval(true)

		if (calcInterval) {
			const interval = setInterval(() => {
				calcGohome(valuesParsed)
			}, 1000 * 60)
			return () => clearInterval(interval)
		}
	}, [calcInterval, data, setValue])

	const startTimeValue = watch('startTime')
	const maxTimeValue = watch('maxTime')
	const minTimeValue = watch('minTime')
	const lunchTimeValue = watch('lunchTime')

	return (
		<Wrapper direction={ContainerEnumDirection.COL} y={ContainerEnumPosition.TOP}>
			<Form onSubmit={handleSubmit(onSubmit)}>
				<WrapperInput>
					<Input
						name="startTime"
						register={register}
						rules={{ required: true, validate: (value: string): boolean => isValidInput(value) }}
						value={watch('startTime')}
						type={InputEnumType.TEXT}
						placeholder="Type your start working time"
						label="Type your start working time"
						error={errors.startTime && createHint(startTimeValue, 'startTime')}
						info="Example with start working time: 7:00"
					/>
				</WrapperInput>
				<WrapperInput>
					<Input
						name="maxTime"
						register={register}
						rules={{ required: true, validate: (value: string): boolean => Number(value) > 0 }}
						value={watch('maxTime')}
						type={InputEnumType.NUMBER}
						placeholder="Type your max working time"
						label="Type your max working time"
						error={errors.maxTime && createHint(maxTimeValue, 'maxTime')}
						info="Example with max working time in hours: 8"
					/>
				</WrapperInput>
				<WrapperInput>
					<Input
						name="minTime"
						register={register}
						rules={{ required: true, validate: (value: string): boolean => Number(value) > 0 }}
						value={watch('minTime')}
						type={InputEnumType.NUMBER}
						placeholder="Type your min working time"
						label="Type your min working time"
						error={errors.minTime && createHint(minTimeValue, 'minTime')}
						info="Example with min working time in hours: 6"
					/>
				</WrapperInput>
				<WrapperInput>
					<Input
						name="lunchTime"
						register={register}
						rules={{ required: true, validate: (value: string): boolean => Number(value) > 0 }}
						value={watch('lunchTime')}
						type={InputEnumType.NUMBER}
						placeholder="Type your lunch time"
						label="Type your lunch time"
						error={errors.lunchTime && createHint(lunchTimeValue, 'lunchTime')}
						info="Example with lunch time in minutes: 30"
					/>
				</WrapperInput>
				<WrapperButton>
					<ButtonSubmit type={ButtonEnumType.SUBMIT}>Submit</ButtonSubmit>
					<ButtonReset type={ButtonEnumType.BUTTON} onClick={handleReset}>
						Reset
					</ButtonReset>
				</WrapperButton>
				{showResults && results && <Results results={results} />}
			</Form>
			<Links />
		</Wrapper>
	)
}
