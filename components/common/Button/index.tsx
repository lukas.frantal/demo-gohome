import type { FC } from 'react'
import styled, { css } from 'styled-components'

export enum ButtonEnumType {
	SUBMIT = 'submit',
	BUTTON = 'button',
}

export type ButtonProps = {
	onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void
	className?: string
	hover?: boolean
	type: ButtonEnumType
	children: React.ReactNode
}

const Wrapper = styled.button<Pick<ButtonProps, 'hover'>>`
	display: inline-block;
	text-transform: uppercase;
	cursor: pointer;
	max-width: 280px;
	width: 100%;
	padding: 15px 20px;
	text-align: center;
	background-color: ${({ theme }) => theme.colors.blue};
	color: ${({ theme }) => theme.colors.white};
	border: 2px solid ${({ theme }) => theme.colors.blue};
	border-radius: 18px;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	text-align: center;

	${({ theme }) => theme.breakpoints.desktop} {
		${({ hover }) =>
			hover &&
			css`
				&:hover {
					background-color: ${({ theme }) => theme.colors.white};
					color: ${({ theme }) => theme.colors.blue};
				}

				&:focus {
					outline: 0;
				}
			`}
	}
`

export const Button: FC<ButtonProps> = ({ className, type, onClick, hover, children }: ButtonProps) => (
	<Wrapper className={className} type={type} onClick={onClick} hover={hover || true}>
		{children}
	</Wrapper>
)
