import type { FC } from 'react'
import { default as LinkNext } from 'next/link'
import styled from 'styled-components'

interface LinkProps {
	className?: string
	href: string
	locale?: string
	title?: string
	active?: boolean
	scroll?: boolean
	replaceHrefTo?: string
	target?: 'blank' | string
	onClick?: () => void
}

const Wrapper = styled.a<Pick<LinkProps, 'active'>>`
	cursor: pointer;
	user-select: none;
	position: relative;
	transition: color 1s;
	font-weight: normal;
	font-size: inherit;
	color: ${({ active, theme }) => active && theme.colors.blue};

	&:after {
		transition: all 0.5s;
		position: absolute;
		bottom: 0;
		left: 0;
		right: 0;
		margin: 0 auto;
		content: '';
		background: ${({ theme }) => theme.colors.blue};
		height: 1px;
		width: ${({ active }) => (active ? '100%' : '0%')};
		opacity: ${({ active }) => (active ? 1 : 0)};

		${({ theme }) => theme.breakpoints.tv5k} {
			height: 2px;
		}
	}

	&:hover {
		text-decoration: none;
		color: ${({ theme }) => theme.colors.blue};

		&:after {
			opacity: 1;
			width: 100%;
		}
	}
`

export const Link: FC<LinkProps> = ({ className, children, href, onClick, replaceHrefTo, scroll = true, active, locale, target, title }) => (
	<LinkNext href={href} as={replaceHrefTo} scroll={scroll} locale={locale} passHref>
		<Wrapper className={className} onClick={onClick} active={active} data-effect={children} target={target} title={title}>
			{children}
		</Wrapper>
	</LinkNext>
)
