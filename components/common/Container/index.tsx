//@ts-check
import React from 'react'
import styled from 'styled-components'

export enum ContainerEnumPosition {
	TOP = 'flex-start',
	LEFT = 'flex-start',
	BOTTOM = 'flex-end',
	RIGHT = 'flex-end',
	CENTER = 'center',
}

export enum ContainerEnumDirection {
	ROW = 'row',
	COL = 'column',
}

export type ContainerProps = {
	children: React.ReactNode
	className?: string
	direction?: ContainerEnumDirection
	x?: ContainerEnumPosition
	y?: ContainerEnumPosition
}

const Wrapper = styled.div<ContainerProps>`
	position: relative;
	display: flex;
	flex-direction: ${({ direction }): string => direction || ContainerEnumDirection.ROW};
	justify-content: ${({ direction, x, y }): string =>
		direction === ContainerEnumDirection.ROW ? x || ContainerEnumPosition.CENTER : y || ContainerEnumPosition.CENTER};
	align-items: ${({ direction, x, y }): string =>
		direction === ContainerEnumDirection.ROW ? y || ContainerEnumPosition.CENTER : x || ContainerEnumPosition.CENTER};
	width: 100%;
`

export const Container: React.FC<ContainerProps> = ({ children, className, direction, x, y }: ContainerProps) => (
	<Wrapper className={className} direction={direction} x={x} y={y}>
		{children}
	</Wrapper>
)
