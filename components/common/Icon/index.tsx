import type { FC } from 'react'
import styled from 'styled-components'
import dataJsonIcons from './selection.json'

const Wrapper = styled.i`
	display: inline-block;
	position: relative;
	z-index: 1;
	width: 30px;
	height: 30px;

	&:before {
		color: inherit;
		font-size: inherit;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
`

export enum IconEnumName {
	CLOSE = 'close',
	ARROWLEFT = 'arrow-left',
}

type ListIcon = typeof dataJsonIcons
export const getListIcons = (): ListIcon => dataJsonIcons

interface IconProps {
	className?: string
	name: IconEnumName
	onClick?: () => void
}

export const Icon: FC<IconProps> = ({ name, className = '', onClick }) => (
	<Wrapper onClick={onClick} className={`icon icon-${name} ${className} rotate`} />
)
