import type { FC } from 'react'
import { useState } from 'react'
import type { RegisterOptions, UseFormRegister } from 'react-hook-form'
import styled, { css } from 'styled-components'

export enum InputEnumType {
	TEXT = 'text',
	EMAIL = 'email',
	NUMBER = 'number',
	PASSWORD = 'password',
	TEL = 'tel',
}

type TimeValuesProps = {
	startTime: string
	maxTime: string
	minTime: string
	lunchTime: string
}

export interface InputProps {
	type?: InputEnumType
	name: 'startTime' | 'maxTime' | 'minTime' | 'lunchTime'
	rules?: RegisterOptions
	value?: string
	label?: string
	register: UseFormRegister<TimeValuesProps>
	placeholder?: string
	error?: string | boolean
	info?: string
	className?: string
}

const Wrapper = styled.div`
	position: relative;
	color: ${({ theme }) => theme.colors.blue};
	width: 100%;
	height: 50px;
`

const Title = styled.input<Pick<InputProps, 'value' | 'error'> & { active: boolean }>`
	position: absolute;
	overflow: hidden;
	z-index: 1;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	width: 100%;
	padding: 15px 25px;
	font-size: 16px;
	height: 50px;
	background-color: ${({ theme }) => theme.colors.white};
	color: ${({ theme }) => theme.colors.black};
	border: 1px solid rgba(0, 0, 0, 0.5);
	border-radius: 5px;
	transition: all 0.2s;

	::placeholder {
		color: rgba(0, 0, 0, 0.5);
		opacity: ${({ value }) => value && !!value.length && 0};
		opacity: ${({ active }) => !active && 0};
	}

	${({ active }) =>
		active &&
		css`
			border-color: ${({ theme }) => theme.colors.blue};
			outline: 0;
			border-width: 2px;
		`}

	&:focus {
		outline: 0;
	}

	border-color: ${({ error, theme }) => error && theme.colors.red};
`

const Lable = styled.label<Pick<InputProps, 'error'> & { active: boolean }>`
	color: ${({ theme }) => theme.colors.blue};
	position: absolute;
	z-index: 3;
	background-color: ${({ theme }) => theme.colors.white};
	top: 50%;
	left: 15px;
	padding: 5px;
	text-align: left;
	color: rgba(0, 0, 0, 0.5);
	transform: translateY(-50%);
	transition: all 0.2s;

	${({ active }) =>
		active &&
		css`
			width: auto;
			color: ${({ theme }) => theme.colors.blue};
			transform: translate(-5px, -33px) scale(0.8);
			padding: 0 5px;
		`}

	color: ${({ error, theme }) => error && theme.colors.red};
`

const Information = styled.span<Pick<InputProps, 'error'>>`
	display: block;
	width: 100%;
	text-align: left;
	color: ${({ theme }) => theme.colors.blue};
	background-color: ${({ theme }) => theme.colors.white};
	padding: 5px 10px;
	font-size: 12px;
	${({ error }) =>
		error &&
		css`
			color: ${({ theme }) => theme.colors.red};
		`}
`

export const Input: FC<InputProps> = ({ className, register, type, name, value, placeholder, error = false, info, label, rules = {} }) => {
	const [activeLabel, setActiveLabel] = useState(false)
	const active = activeLabel || (value && value.length > 0) || false

	return (
		<>
			<Wrapper className={className}>
				<Title
					{...register(name, rules)}
					type={type}
					defaultValue={value}
					active={active}
					id={name}
					error={error}
					placeholder={placeholder}
					onFocus={() => setActiveLabel(true)}
					onBlur={() => setActiveLabel(false)}
				/>
				<Lable active={active} htmlFor={name} error={error}>
					{label}
				</Lable>
			</Wrapper>
			<Information error={error}>{error ? error : info}</Information>
		</>
	)
}
