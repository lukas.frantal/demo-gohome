import type { FC } from 'react'
import styled from 'styled-components'
import { Container, ContainerEnumDirection } from '../common'

const Wrapper = styled(Container)<{ mark?: boolean }>`
	justify-content: space-between;
	color: ${({ theme, mark }) => mark && theme.colors.red};
	background-color: ${({ theme }) => theme.colors.white};
	max-width: 350px;
`

const Text = styled.span`
	text-align: left;
	font-size: 14px;
	padding: 7px;
	font-weight: bold;

	${({ theme }) => theme.breakpoints.tablet} {
		font-size: 16px;
	}
`

type ResultsProps = {
	results: {
		actual: {
			hours: string
			minutes: string
		}
		start: {
			hours: string
			minutes: string
		}
		min: {
			done: boolean
			hours: string
			minutes: string
			saldo: {
				hours: string
				minutes: string
			}
		}
		max: {
			done: boolean
			hours: string
			minutes: string
			saldo: {
				hours: string
				minutes: string
			}
		}
	}
}

export const Results: FC<ResultsProps> = ({ results }) => (
	<Container direction={ContainerEnumDirection.COL}>
		<Wrapper>
			<Text>Actual time: </Text>
			<Text>
				{results.actual.hours}:{results.actual.minutes}
			</Text>
		</Wrapper>
		<Wrapper>
			<Text>Your start working time: </Text>
			<Text>
				{results.start.hours}:{results.start.minutes}
			</Text>
		</Wrapper>
		<Wrapper mark={results.min.done}>
			<Text>You can go home by min time at: </Text>
			<Text>
				{results.min.hours}:{results.min.minutes}
			</Text>
		</Wrapper>
		{results.min.done && (
			<Wrapper mark>
				<Text>Your actual saldo for min time is: </Text>
				<Text>
					-{results.min.saldo.hours}:{results.min.saldo.minutes}
				</Text>
			</Wrapper>
		)}
		<Wrapper mark={results.max.done}>
			<Text>You can go home by max time at: </Text>
			<Text>
				{results.max.hours}:{results.max.minutes}
			</Text>
		</Wrapper>
		<Wrapper mark={results.max.done}>
			<Text>Your actual saldo is: </Text>
			<Text>
				{results.max.done ? '-' : ''}
				{results.max.saldo.hours}:{results.max.saldo.minutes}
			</Text>
		</Wrapper>
	</Container>
)
