import type { FC } from 'react'
import styled from 'styled-components'

const Wrapper = styled.header`
	background-color: ${({ theme }) => theme.colors.black};
	text-align: center;
	width: 100%;
	font-weight: bold;
	font-size: 24px;
	padding: 20px 0;
`

export const Header: FC = () => (
	<Wrapper>
		<h1>Demo gohome</h1>
	</Wrapper>
)
