import type { FC } from 'react'
import styled from 'styled-components'
import { Container, Link as LinkC } from '../common'

const Wrapper = styled.footer`
	background-color: ${({ theme }) => theme.colors.black};
	padding: 0 15px 15px;
	width: 100%;
	font-size: 12px;
	font-weight: bold;
`

const Content = styled(Container)`
	max-width: 600px;
	justify-content: space-between;
	margin: 0 auto;
	flex-wrap: wrap;
`

const Item = styled.li`
	border-right: 1px solid ${({ theme }) => theme.colors.blue};
	padding: 0 10px;
	margin-top: 15px;
`

const LastItem = styled(Item)`
	border-right: 0;
`

const Description = styled.div`
	margin-top: 15px;
	padding: 0 10px;
`

const List = styled.ul`
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
`

const Link = styled(LinkC)`
	padding: 5px;
`

export const Footer: FC = () => (
	<Wrapper>
		<Content>
			<List>
				<Item>
					<Link href="https://www.npmjs.com/settings/lukas-frantal/packages">npmjs</Link>
				</Item>
				<Item>
					<Link href="https://gitlab.com/lukas.frantal">GitLab</Link>
				</Item>
				<Item>
					<Link href="https://github.com/frantallukas10?tab=repositories">GitHub</Link>
				</Item>
				<LastItem>
					<Link href="mailto:frantal.lukas10@gmail.com">Email</Link>
				</LastItem>
			</List>
			<Description>
				© 2020 <Link href="https://www.linkedin.com/in/lukas-frantal/">Ing. Lukáš Frantál</Link>
			</Description>
		</Content>
	</Wrapper>
)
