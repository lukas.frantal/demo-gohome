import type { FC } from 'react'
import styled from 'styled-components'
import Image, { ImageProps } from 'next/image'
import { Container, Link as LinkC } from '../common'
import links from '../../public/mock/links.json'

const Wrapper = styled.header`
	text-align: center;
	width: 100%;
`

const Title = styled.h1`
	background-color: ${({ theme }) => theme.colors.black};
	font-weight: bold;
	font-size: 18px;
	padding: 10px 0;
`

const WrapperLinks = styled(Container)`
	flex-wrap: wrap;
	padding: 10px;
	gap: 60px;

	${({ theme }) => theme.breakpoints.desktop} {
		padding: 30px;
	}
`

const Link = styled(LinkC)`
	font-weight: bold;
	border-radius: 5px;
	height: 60px;
	display: inline-block;
	flex: 1;
	min-width: 100px;

	&::after {
		display: none;
	}

	&:hover {
		opacity: 0.7;
	}

	${({ theme }) => theme.breakpoints.desktop} {
		flex-direction: row;
		height: 60px;
		min-width: auto;
	}
`

const Img = styled(Image)<ImageProps>`
	font-weight: bold;
	object-fit: contain;
	background-color: ${({ theme }) => theme.colors.white};
`

export const Links: FC = () => (
	<Wrapper>
		<Title>Used Technologies:</Title>
		<WrapperLinks>
			{links.map(item => (
				<Link key={item.name} href={item.href} target="_blank" title={item.name}>
					<Img src={item.src} alt={item.name} layout="fill" />
				</Link>
			))}
		</WrapperLinks>
	</Wrapper>
)
