module.exports = {
	root: true,
	env: {
		browser: true,
		node: true,
		es6: true,
		jest: true,
	},
	parser: '@typescript-eslint/parser',
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:@typescript-eslint/recommended-requiring-type-checking',
		'plugin:prettier/recommended',
		'plugin:import/typescript',
		'plugin:import/errors',
		'plugin:import/warnings',

		'plugin:react/recommended',
		'plugin:react-hooks/recommended',
		'plugin:jsx-a11y/recommended',
		'plugin:react-hooks/recommended',
	],
	plugins: ['@typescript-eslint', 'prettier', 'import', 'react-hooks'],
	parserOptions: {
		createDefaultProgram: true,
		project: './tsconfig.json',
		sourceType: 'module',
		ecmaVersion: 2021,
		ecmaFeatures: {
			jsx: true,
		},
	},
	settings: {
		'import/resolver': {
			node: {
				extensions: ['.js', '.jsx', '.ts', '.tsx'],
			},
		},
		react: {
			version: 'detect',
		},
	},
	rules: {
		'prettier/prettier': ['error', {}, { usePrettierrc: true }],
		'arrow-body-style': 'error',
		'prefer-arrow-callback': 'error',
		'import/first': 'error',
		'import/newline-after-import': 'error',
		'@typescript-eslint/ban-ts-ignore': 'off',
		'arrow-parens': 'off',
		'import/export': 'error',
		'max-len': ['error', { code: 150 }],
		'@typescript-eslint/no-unused-expressions': 'off',
		'@typescript-eslint/comma-dangle': [
			'error',
			{
				arrays: 'always-multiline',
				enums: 'always-multiline',
				exports: 'always-multiline',
				functions: 'always-multiline',
				imports: 'always-multiline',
				objects: 'always-multiline',
			},
		],
		'import/no-duplicates': 'error',
		'import/no-deprecated': 'error',
		'import/named': 'error',
		'import/namespace': 'error',
		'import/default': 'error',
		'import/order': ['error', { groups: ['index', 'builtin', 'external', 'internal', 'parent', 'sibling'] }],
		'@typescript-eslint/no-unused-vars': 'error',
		'@typescript-eslint/no-unsafe-return': 'off',
		'@typescript-eslint/explicit-function-return-type': [
			'warn',
			{
				allowExpressions: true,
				allowConciseArrowFunctionExpressionsStartingWithVoid: true,
			},
		],
		'@typescript-eslint/no-empty-interface': [
			'error',
			{
				allowSingleExtends: true,
			},
		],
		'@typescript-eslint/unbound-method': 'warn',
		'@typescript-eslint/no-unsafe-assignment': 'warn',

		'no-console': ['error', { allow: ['warn', 'error'] }],
		'react-hooks/rules-of-hooks': 'error',
		'react-hooks/exhaustive-deps': 'warn',
		'react/prop-types': 'off',
		'react/react-in-jsx-scope': 'off',
		'jsx-a11y/anchor-is-valid': 'off',
	},
}
