export const debounce = (callback: () => void, wait: number): (() => void) => {
	let timer: number | undefined = undefined
	return (...args) => {
		window.clearTimeout(timer)
		timer = window.setTimeout(() => {
			callback(...args)
		}, wait)
	}
}
