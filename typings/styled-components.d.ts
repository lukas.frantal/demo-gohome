import { theme } from '../styles/theme'

type Custom = typeof theme

declare module 'styled-components' {
	export interface DefaultTheme extends Custom {}
}
