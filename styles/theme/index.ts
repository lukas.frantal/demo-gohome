const breakpoints = {
	tablet: '@media screen and (min-width: 600px)',
	desktop: '@media screen and (min-width: 1200px)',
	tv5k: '@media screen and (min-width: 1920px)',
}
export const theme = {
	fontFamily: 'Arial, Helvetica, sans-serif',
	colors: {
		white: '#fff',
		black: '#000',
		blue: '#00adb5',
		red: '#E32636',
	},
	breakpoints,
}
