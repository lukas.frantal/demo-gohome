import type { FC } from 'react'
import { createGlobalStyle, ThemeProvider } from 'styled-components'
import { Reset } from 'styled-reset'
import { theme } from './theme'

export const GlobalStyle = createGlobalStyle`
  html, body, body > div {
    font-family: ${({ theme }) => theme.fontFamily};
    color: ${({ theme }) => theme.colors.blue};
    height: 100%;
    background-color: ${({ theme }) => theme.colors.white};
    background-image: ${({ theme }) =>
			`radial-gradient(${theme.colors.white} 1px, transparent 1px), radial-gradient(${theme.colors.white} 1px, transparent 1px)`};
    background-position: 0 0, 25px 25px;
  }
	
	* {
		outline: 0;
		box-sizing: border-box;
	}

  a {
    color: inherit;
    display: inline-block;
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }
  }
`

export const Styler: FC = props => (
	<ThemeProvider theme={theme}>
		<Reset />
		<GlobalStyle />
		{props.children}
	</ThemeProvider>
)
